﻿using System;

namespace RecursionMaxNumber
{
    public class Math
    {
        public int MaxNumber(int[] array)
        {
            var x = int.MinValue;
            foreach (var number in array)
            {
                if (number > x)
                {
                    x = number;
                }
            }
            return x;
        }
    }
}
