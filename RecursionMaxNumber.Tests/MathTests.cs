﻿using NUnit.Framework;

namespace RecursionMaxNumber.Tests
{
    public class MathTests
    {
        [TestCase(new int[] { 1, 2, 4, 6, 10, 8 }, 10)]
        [TestCase(new int[] { -10, 2, 1, 8 }, 8)]
        [TestCase(new int[] { 10, 4, 8 }, 10)]
        [TestCase(new int[] { 1, 8 }, 8)]
        [TestCase(new int[] { -10, -1, -2, -88 }, -1)]
        [TestCase(new int[] { 10 }, 10)]
        [TestCase(new int[] { 8, 87, 11, 0 }, 87)]

        public void Math_Max_Number_Tests(int[] array, int expected)
        {

            var math = new Math();
            Assert.AreEqual(expected, math.MaxNumber(array));
        }
    }
}
